![](images/screenshot.png)

Intro
-----

`pac` is a NeoVim package manager, written in [crystal].
It utilizes the internal NeoVim's mechanism, called *package*, so the impact on the loading speed is minimal
(see `:help package` for more information).
The only fully supported platform is GNU Linux, but it should mostly work with OSX as well.
Vim is not supported, you can use [pack] for it.

Dependencies
------------

- git (proved to work on version 2.11)
- xdg-open (to open urls with browser)
- nvim (to generate helptags)

Interface
---------

```
Usage: pac <command> [options] [url]
Commands:
  add    URL1 [URL2...]     - Add packages
  browse NAME1 [NAME2...]   - View package's url with browser
  list   [NAMES]            - List packages
  remove NAME1 [NAME2...]   - Remove packages
  set    NAME1 [NAME2...]   - Set package's attributes (see options)
  update NAME1 [[NAME2...]] - Update packages (except the frozen ones)
  view   NAME               - View the package's repo vith editor
Options:
    -h                               Show this help
    -f                               Freeze the package(s)
    -F                               Unfreeze the package(s)
    -a                               Autoload the package(s)
    -s                               Stick to the last tag
    -S                               Unstick from the last tag
    -o                               Make the package(s) optional
    -O CMD                           Load the package on command (implies -o)
```

Notes
-----

- It doesn't need to type in the full command, once the typed chars are enough to denote a command, it will be accepted.
- A NAME is treated as a [PCRE] regular expression.
- To speed-up the process, adding or updating packages happens in parallel.
- Completion for ZSH can be found in `contrib/`.

[crystal]: https://crystal-lang.org/
[pack]: https://github.com/maralla/pack
[PCRE]: http://www.pcre.org/original/doc/html/pcresyntax.html
