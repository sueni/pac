#compdef pac
# https://wikimatze.de/writing-zsh-completion-for-padrino
# https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org

local package_files=(~d/nvim/pack/.db/*yml)
local packages=(${package_files:t:r})

local commands=(
"add:Add packages"
"browse:View package's url with browser"
"list:List packages"
"remove:Remove packages"
"set:Set package's attributes (see options)"
"update:Update packages (except the frozen ones)"
"view:View the package's repo with editor"
)

_arguments -s -S -C '1:command:->cmd' '*::package:->packages'

case "$state" in
	(cmd) _describe 'command' commands ;;
	(packages)
		case "$line[1]" in
			(browse|remove|update|view)
				_values -w 'packages' $packages ;;
			(add)
				_arguments \
					'*:url:( )' \
					'-h[Help]' \
					'-f[Freeze the package(s)]' \
					'-F[Unfreeze the package(s)]' \
					'-a[Autoload the package(s)]' \
					'-o[Make the package(s) optional]' \
					'-O+[Load the package on command (implies -o)]' \
					'-s[Stick to the last tag]' \
					'-S[Unstick from the last tag]' ;;
			(list)
				_arguments \
					'-h[Help]' \
					'-f[Consider frozen packages only]' \
					'-F[Consider unfrozen packages only]' \
					'-a[Consider autoloaded packages only]' \
					'-o[Consider optional packages only]' ;;
			(set)
				_arguments \
					'*:package: _values -w "packages" $packages' \
					'-h[Help]' \
					'-f[Freeze the package(s)]' \
					'-F[Unfreeze the package(s)]' \
					'-a[Autoload the package(s)]' \
					'-o[Make the package(s) optional]' \
					'-O+[Load the package on command (implies -o)]' \
					'-s[Stick to the last tag]' \
					'-S[Unstick from the last tag]' ;;
		esac
esac
