module Color
  extend self

  macro add_color(name, *codes)
    def {{name}}(str)
      wrap(str, {{codes}})
    end
  end

  private def wrap(str, color_codes)
    return str unless STDOUT.tty?
    "\e[#{color_codes.join(';')}m#{str}\e[0m"
  end

  add_color err, 1, 91
  add_color header, 7
  add_color err_header, 7, 91
  add_color ok, 92
  add_color dim, 90
  add_color warn, 7, 33
  add_color ago, 4
end
