require "option_parser"

module CLI
  extend self

  enum Command
    Add
    Browse
    List
    Remove
    Set
    Update
    View
  end

  record Data, options : Options, command : Command

  struct Options
    property optional : Bool?
    property frozen : Bool?
    property on_cmd : String?
    property sticky_last_tag : Bool?
  end

  private def options=(opts : Options)
    @@options = opts
  end

  def parse
    Data.new(parsed_options, parsed_command)
  end

  private def parsed_options
    opts = Options.new

    OptionParser.parse do |parser|
      parser.banner = help
      parser.separator "Options:"
      parser.on("-a", "Autoload the package(s)") { opts.optional = false }
      parser.on("-h", "Show this help") { abort parser }
      parser.on("-f", "Freeze the package(s)") { opts.frozen = true }
      parser.on("-F", "Unfreeze the package(s)") { opts.frozen = false }
      parser.on("-s", "Stick to the last tag") { opts.sticky_last_tag = true }
      parser.on("-S", "Unstick from the last tag") { opts.sticky_last_tag = false }

      parser.on("-o", "Make the package(s) optional") { opts.optional = true }
      parser.on("-O CMD",
        "Load the package on command (implies -o)") do |o|
        opts.optional = true
        opts.on_cmd = o
      end

      parser.invalid_option do |flag|
        Info.err "ERROR: #{flag} is not a valid option", ""
        abort parser
      end

      parser.missing_option do |flag|
        Info.err "ERROR: #{flag} needs an argument", ""
        abort parser
      end
    end

    opts
  end

  private def help
    <<-END
    Usage: pac <command> [options] [url]
    Commands:
      add    URL1 [URL2...]     - Add packages
      browse NAME1 [NAME2...]   - View package's url with browser
      list   [NAME(S)]          - List packages
      remove NAME1 [NAME2...]   - Remove packages
      set    NAME1 [NAME2...]   - Set package's attributes (see options)
      update [NAME1 [NAME2...]] - Update packages
      view   NAME               - View the package's repo vith editor
    END
  end

  private def parsed_command
    abort help unless (cmd = ARGV.shift?)

    matches = [] of Command
    Command.each do |member|
      mem = member.to_s.downcase
      matches << member if mem.starts_with?(cmd)
    end

    case matches.size
    when 1 then matches.last
    else        abort help
    end
  rescue ArgumentError
    abort help
  end
end
