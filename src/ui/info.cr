module Info
  extend self

  class_getter exit_code = 0

  @@err_pad : String = Color.err(" -> ")

  def ok(package, message)
    printf "%s: %s\n", package, Color.ok(message)
  end

  def err(*messages)
    @@exit_code = 1
    messages.each { |msg| STDERR.puts Color.err(msg.strip) }
  end

  def err(message, package : String)
    @@exit_code = 1

    STDERR.printf(
      "%s:\n%s %s\n",
      Color.err_header(package),
      @@err_pad,
      message.gsub("\n", "\n#{@@err_pad}")
    )
  end

  def err(message, packages : Array)
    @@exit_code = 1
    STDERR.puts "#{Color.err(message)}:"
    packages.each { |package| STDERR.puts "#{@@err_pad}#{package}" }
  end
end
