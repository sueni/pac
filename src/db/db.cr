struct DB
  DIR = "#{Pac::STORE}/.db"
  Dir.mkdir_p DIR unless Dir.exists? DIR

  @items : Array(DBItem)

  def initialize
    @items = Dir["#{DIR}/*yml"].sort.map do |f|
      DBItem.from_yaml(File.read f)
    end
  end

  def apply_filter(options)
    filter = Filter.new(options)
    filter.apply_to(@items)
  end

  def each_match(argv, options)
    if argv.empty?
      @items.each do |item|
        if options.frozen
          yield item if item.frozen?
        else
          yield item unless item.frozen?
        end
      end
    else
      argv.each do |request|
        @items.select(&.name.=~ Regex.new(
          request,
          Regex::Options::IGNORE_CASE
        )).each { |item| yield item }
      end
    end

  end

  def each_match!(argv)
    Info.err "Provide package(s) name(s)" if argv.empty?

    argv.each do |request|
      extract = @items.select(&.name.=~ Regex.new(
        request,
        Regex::Options::IGNORE_CASE))
      case extract.size
      when 0 then Info.err "No package matched", request
      when 1 then yield extract.last
      else
        Info.err "Ambiguous packages match", extract.map(&.name)
      end
    end
  end

  def delete(item : DBItem)
    File.delete item.path if File.exists? item.path
  end

  private def name(file)
    File.basename(file).rchop(".yml")
  end
end
