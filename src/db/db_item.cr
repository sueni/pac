require "yaml"
require "uri"

struct DBItem
  include YAML::Serializable

  property name : String
  property host : String
  property url : String
  property? optional : Bool
  property? frozen : Bool
  property on_cmd : String?
  property? sticky_last_tag : Bool
  property last_updated = Time.local

  def initialize(
    @name : String,
    @host : String,
    @url : String,
    @optional : Bool,
    @frozen : Bool,
    @on_cmd : String?,
    @sticky_last_tag : Bool
  )
  end

  def save
    self.last_updated = Time.local
    File.write(path, self.to_yaml)
  end

  def path
    "#{DB::DIR}/#{name}.yml"
  end

  def remote
    uri = URI.parse(url)
    "#{uri.host}#{uri.path}"
  end

  def repo
    "#{Pac::STORE}/#{host}/#{optional? ? "opt" : "start"}/#{name}"
  end
end
