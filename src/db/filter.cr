struct Filter
  def initialize(@options : CLI::Options)
  end

  def apply_to(items : Array(DBItem))
    filter_frozen items
    filter_optional items
  end

  private def filter_frozen(items)
    items.select! do |item|
      case @options.frozen
      when Nil   then true
      when false then !item.frozen?
      when true  then item.frozen?
      end
    end
  end

  private def filter_optional(items)
    items.select! do |item|
      case @options.optional
      when Nil   then true
      when false then !item.optional?
      when true  then item.optional?
      end
    end
  end
end
