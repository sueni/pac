module LocalPackage
  struct Browse
    def initialize(@item : DBItem)
    end

    def call
      unless @item.url.starts_with?("http")
        return puts Color.warn("#{@item.name}: url is not browsable")
      end

      Process.new("xdg-open", {@item.url})
      Info.ok @item.name, "sent to browser"
    end
  end
end
