module LocalPackage
  struct Update
    @log_master : String?

    def initialize(@item : DBItem)
    end

    def call
      fetch && begin
        report_news
        merge_master
        Helptags.generate_for @item.repo
        checkout_last_tag if @item.sticky_last_tag?
        collect_garbage if rand > 0.5
        @item.save
      end
    end

    private def fetch
      Git.fetch @item.repo
      !(@log_master = Git.log_master(@item.repo)).empty?
    end

    private def report_news
      puts Color.header("#{@item.name}")
      puts @log_master
    end

    private def merge_master
      Git.checkout @item.repo, GitPointer.master
      success, error_msg = Git.merge_master(@item.repo)
      Info.err error_msg, @item.name unless success
    end

    private def collect_garbage
      Git.gc @item.repo
    end

    private def checkout_last_tag
      puts Color.warn("  Checking out the last tag")
      Git.checkout @item.repo, GitPointer.last_local_tag(@item.repo)
    end
  end
end
