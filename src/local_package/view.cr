module LocalPackage
  struct View
    def initialize(@item : DBItem)
    end

    def call
      system "editor #{@item.repo}"
    end
  end
end
