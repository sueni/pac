require "file_utils"

module LocalPackage
  struct Set
    @item : DBItem
    @options : CLI::Options
    @changed = false

    def initialize(@item, @options)
    end

    def call
      change_type
      change_freeze
      change_command
      change_last_tag_stick
      @item.save if @changed
    end

    private def change_type
      optional = @options.optional
      return if optional.nil? || optional == @item.optional?
      shift_path(path, path(optional))
      @item.optional = optional
      Info.ok @item.name, "type set to <#{type optional}>"
      @changed = true
    end

    private def change_freeze
      frozen = @options.frozen
      return if frozen.nil? || frozen == @item.frozen?
      @item.frozen = frozen
      Info.ok @item.name, "package #{"un" unless frozen}frozen"
      @changed = true
    end

    private def change_command
      return if @options.on_cmd == @item.on_cmd
      add_cmd if @options.on_cmd && !@item.on_cmd
      remove_cmd if !@options.on_cmd && @item.on_cmd
      change_cmd if @options.on_cmd != @item.on_cmd
      @changed = true
    end

    private def change_cmd
      if (on_cmd = @options.on_cmd)
        LazyCommand.change(on_cmd, @item.name)
        @item.on_cmd = on_cmd
        Info.ok @item.name, "on-demand command changed to <#{on_cmd}>"
      end
    end

    private def add_cmd
      if (on_cmd = @options.on_cmd)
        LazyCommand.add(on_cmd, @item.name)
        @item.on_cmd = on_cmd
        Info.ok @item.name, "attached on-demand command <#{on_cmd}>"
      end
    end

    private def remove_cmd
      if (on_cmd = @item.on_cmd)
        LazyCommand.remove(on_cmd)
        @item.on_cmd = nil
        Info.ok @item.name, "removed on-demand command <#{on_cmd}>"
      end
    end

    private def change_last_tag_stick
      sticky = @options.sticky_last_tag
      return if sticky.nil? || sticky == @item.sticky_last_tag?
      @item.sticky_last_tag = sticky
      Git.checkout @item.repo, (sticky ? GitPointer.last_local_tag(@item.repo) : GitPointer.master)
      Info.ok @item.name, "package #{"un" unless sticky}stick"
      @changed = true
    end

    private def path(optional = @item.optional?)
      "#{Pac::STORE}/#{@item.host}/#{type(optional)}/#{@item.name}"
    end

    private def type(optional)
      optional ? "opt" : "start"
    end

    private def shift_path(source, destination)
      DirHelper.grow_parent_branch to: destination
      FileUtils.mv source, destination
      DirHelper.cut_empty_branch source
    end
  end
end
