require "file_utils"

module LocalPackage
  struct Remove
    def initialize(@db : DB)
    end

    def call(item)
      remove_repo item
      remove_lazy_command item
      @db.delete item
      Info.ok item.name, "package removed"
    end

    private def remove_repo(item)
      DirHelper.with_empty_branch_cut item.repo do
        FileUtils.rm_r item.repo if Dir.exists? item.repo
      end
    end

    def remove_lazy_command(item)
      if (on_cmd = item.on_cmd)
        LazyCommand.remove(on_cmd)
      end
    end
  end
end
