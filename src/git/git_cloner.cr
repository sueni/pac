abstract struct GitCloner
  abstract def clone(url : String, to dir : String)
end

struct LastTagCloner < GitCloner
  def clone(url, to dir)
    Git.clone(url, dir) do
      {"clone", "--branch", GitPointer.last_remote_tag(url),
       "--single-branch", "--depth=1", url, dir}
    end
  end
end

struct LastCommitCloner < GitCloner
  def clone(url, to dir)
    Git.clone(url, dir) do
      {"clone", "--depth=1", url, dir}
    end
  end
end
