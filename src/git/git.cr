module Git
  extend self

  def clone(url, dir)
    error_msg = Process.run(
      "git",
      with self yield,
      &.error.gets_to_end
    )

    {$?.success?, error_msg}
  end

  def fetch(repo)
    Process.new("git", {"fetch"}, chdir: repo).wait
  end

  def gc(repo)
    Process.new("git", {"gc"}, chdir: repo).wait
  end

  def log_master(repo)
    Process.run(
      "git",
      {"log", "master..origin/master", "--color=always",
       "--oneline", "--pretty=%C(auto)- %h%d %s%Creset"},
      chdir: repo,
      &.output.gets_to_end
    )
  end

  def merge_master(repo)
    error_msg = Process.run(
      "git",
      {"merge", "--ff-only", "origin/master"},
      chdir: repo,
      &.error.gets_to_end)

    {$?.success?, error_msg}
  end

  def checkout(repo, point : String)
    Process.new("git", {"checkout", point}, chdir: repo).wait
  end
end
