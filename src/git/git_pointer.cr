module GitPointer
  extend self

  def last_remote_tag(url)
    Process.run(
      "git",
      {"ls-remote", "--tags", "--refs", url},
      &.output.gets_to_end
    )
      .split("\n")[-2]
      .split('/').last
  end

  def master
    "master"
  end

  def last_local_tag(dir)
    Process.run(
      "git",
      {"describe", "--abbrev=0"},
      chdir: dir,
      &.output.gets_to_end.chomp
    )
  end
end
