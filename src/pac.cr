require "./ago"
require "./pac/*"
require "./db/*"
require "./git/*"
require "./local_package/*"
require "./ui/*"

module Pac
  STORE = File.expand_path "~/.config/nvim/pack", home: true
  at_exit { exit Info.exit_code }

  cli = CLI.parse
  db = DB.new
  action = Action.new(cli.options, db)

  case cli.command
  when CLI::Command::List
    db.apply_filter(cli.options)
    action.list
  when CLI::Command::Add    then action.add(fibers: 4)
  when CLI::Command::Browse then action.browse
  when CLI::Command::Remove then action.remove
  when CLI::Command::Set    then action.set
  when CLI::Command::Update then action.update(fibers: 7)
  when CLI::Command::View   then action.view
  else
  end
end
