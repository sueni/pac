module Ago
  NOW = Time.local
  extend self

  def human_readable(last_updated)
    span = NOW - last_updated
    periods(span).find { |k, v| v.to_i > 0 }
      .try { |k, v| "#{v.to_i}#{k}" }
  end

  private def periods(span)
    {
      'w' => span.total_weeks,
      'd' => span.total_days,
      'h' => span.total_hours.ceil,
    }
  end
end
