module DirHelper
  extend self

  def grow_parent_branch(to point)
    parent = File.dirname(point)
    Dir.mkdir_p parent unless Dir.exists? parent
  end

  def with_empty_branch_cut(tip, &block)
    yield
    cut_empty_branch tip
  end

  def cut_empty_branch(tip)
    parent = File.dirname(tip)
    return unless Dir.empty?(parent)
    Dir.delete parent
    cut_empty_branch(parent)
  end
end
