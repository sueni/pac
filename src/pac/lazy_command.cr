module LazyCommand
  extend self

  def add(command : String, package : String)
    init unless File.exists? lazy_loader
    return if lines.any? &.includes?(command)
    lines << command_scheme(command, package)
    save(lines)
  end

  def remove(command : String)
    return unless File.exists? lazy_loader
    cleaned_lines = lines.reject &.includes?(command)
    return clean_up if cleaned_lines.join("\n") == utility
    save(lines.reject &.includes?(command))
  end

  def change(command : String, package : String)
    changed_lines = lines.reject &.includes?(package)
    changed_lines << command_scheme(command, package)
    save(changed_lines)
  end

  private def clean_up
    File.delete lazy_loader
    DirHelper.cut_empty_branch lazy_loader
  end

  private def save(lines : Array(String))
    File.open(lazy_loader, "w") do |f|
      lines.each { |line| f.puts line }
    end
  end

  private def lines
    @@lines ||= File.read_lines(lazy_loader)
  end

  private def init
    DirHelper.grow_parent_branch to: lazy_loader
    File.write(lazy_loader, utility)
  end

  private def lazy_loader
    "#{Pac::STORE}/lazy/start/commands/plugin/pac.vim"
  end

  private def utility
    <<-'EOD'
    " DO NOT EDIT, this file is maintained by nvim package manager
    fun! s:do_cmd(cmd, bang, start, end, args)
      exe printf('%s%s%s %s', (a:start == a:end ? '' : (a:start.','.a:end)), a:cmd, a:bang, a:args)
    endfun
    EOD
  end

  private def command_scheme(command, package)
    <<-EOD
    com! -nargs=* -range -bang #{command} pa #{package} | call s:do_cmd('#{command}', "<bang>", <line1>, <line2>, <q-args>)
    EOD
  end
end
