require "uri"

module URL
  def self.parse(url)
    case url
    when .starts_with?("http")
      URL::HTTP.new(url)
    when .starts_with?("git@")
      URL::Git.new(url)
    else
      abort Info.err "Invalid url: #{url}"
    end
  end

  abstract struct GenericURL
    getter raw

    abstract def host
    abstract def name

    def initialize(@raw : String)
    end
  end

  struct HTTP < GenericURL
    def host
      URI.parse(@raw).host || "unknown"
    end

    def name
      File.basename @raw
    end
  end

  struct Git < GenericURL
    def host
      @raw[/(?<=@).+?(?=:)/] || "unknown"
    end

    def name
      File.basename(@raw).rchop ".git"
    end
  end
end
