struct RemotePackage
  @options : CLI::Options

  def initialize(url, @options)
    @url = URL.parse(url)
    @cloner = @options.sticky_last_tag ? LastTagCloner.new : LastCommitCloner.new
  end

  def add
    success, err_msg = @cloner.clone(@url.raw, to: repo)
    return Info.err(err_msg, @url.name) unless success

    Git.gc repo

    save_item
    Helptags.generate_for repo

    if (on_cmd = @options.on_cmd)
      LazyCommand.add(on_cmd, @url.name)
    end

    Info.ok @url.name, success_message
  end

  private def save_item
    DBItem.new(
      name: @url.name,
      host: @url.host,
      url: @url.raw,
      optional: @options.optional || false,
      frozen: @options.frozen || false,
      on_cmd: @options.on_cmd,
      sticky_last_tag: @options.sticky_last_tag || false,
    ).save
  end

  private def repo
    "#{Pac::STORE}/#{@url.host}/#{type}/#{@url.name}"
  end

  private def type
    @options.optional ? "opt" : "start"
  end

  private def success_message
    String.build do |s|
      s << (@options.optional ? "optional" : "autoloaded")
      s << " sticky" if @options.sticky_last_tag
      s << " package added"
      s << ", with command <#{@options.on_cmd}>" if @options.on_cmd
    end
  end
end
