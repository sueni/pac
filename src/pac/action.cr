struct Action
  @options : CLI::Options
  @db : DB

  def initialize(@options, @db)
    @adding_jobs = Channel(RemotePackage | Nil).new
    @update_jobs = Channel(LocalPackage::Update | Nil).new
  end

  def browse
    @db.each_match!(ARGV) { |item| LocalPackage::Browse.new(item).call }
  end

  def add(fibers)
    return unless ok?
    fibers.times { spawn { adding_worker } }
    ARGV.each { |url| @adding_jobs.send RemotePackage.new(url, @options) }
    fibers.times { @adding_jobs.send nil }
  end

  def list
    # to list all packages by default
    ARGV.push(".") if ARGV.empty?

    @db.each_match(ARGV, @options) do |item|
      printf(
        "%-3s %s %s %s %s\n",
        flags(item),
        item.name,
        Color.dim(item.remote),
        Color.ago(Ago.human_readable item.last_updated),
        item.on_cmd
      )
    end
  end

  def remove
    remover = LocalPackage::Remove.new(@db)
    @db.each_match!(ARGV) { |item| remover.call(item) }
  end

  def set
    return unless ok?
    @db.each_match!(ARGV) { |item| LocalPackage::Set.new(item, @options).call }
  end

  def update(fibers)
    fibers.times { spawn { update_worker } }
    @db.each_match(ARGV, @options) do |item|
      @update_jobs.send LocalPackage::Update.new(item)
    end
    fibers.times { @update_jobs.send nil }
  end

  def view
    @db.each_match!(ARGV) { |item| LocalPackage::View.new(item).call }
  end

  def ok? : Bool
    if @options.on_cmd && ARGV.size > 1
      Info.err "Will not apply -O to multiple packages"
      return false
    end
    true
  end

  private def flags(item)
    String.build do |str|
      str << 'f' if item.frozen?
      str << 'o' if item.optional? && !item.on_cmd
      str << 'O' if item.on_cmd
      str << 's' if item.sticky_last_tag?
    end
  end

  private def adding_worker
    while (package = @adding_jobs.receive)
      package.add
    end
  end

  private def update_worker
    while (package = @update_jobs.receive)
      package.call
    end
  end
end
