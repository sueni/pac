module Helptags
  def self.generate_for(repo)
    Process.new(
      "nvim",
      {"-u", "NORC", "--noplugin", "--headless", "-es",
       "+helptags #{repo}/doc|q"}
    ).wait
  end
end
